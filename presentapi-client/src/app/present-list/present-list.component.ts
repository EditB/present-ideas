import { Observable } from "rxjs";
import { PresentService } from "../present.service";
import { Present } from "../present";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { Person } from "../person";
import { PersonService } from "../person.service";
import { PageEvent } from '@angular/material/paginator';
import { EventService } from "../event.service";
import { Event } from "../event";
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import { PersonListComponent } from '../person-list/person-list.component';

@Component({
  selector: "app-present-list",
  templateUrl: "./present-list.component.html",
  styleUrls: ["./present-list.component.css"]

})
export class PresentListComponent implements OnInit {

  pageIndex: number = 0;
  pageSize: number = 5;
  lowValue: number = 0;
  highValue: number = 5;

  presents: Observable<Present[]>;
  persons: Observable<Person[]>;
  events: Observable<Event[]>;

  isDataAvailable: boolean = false;

  constructor(public dialog: MatDialog, private presentService: PresentService, private personService: PersonService, private eventService: EventService,
    private router: Router) {
  }

  openDialog() {
    this.dialog.open(PersonListComponent, {
      data: {
        animal: 'panda'
      }
    });
  }

  ngOnInit() {
    this.reloadData().subscribe(
      () => this.isDataAvailable = true
    );
  }

  reloadData() {
    return this.presents = this.presentService.getPresentsList();
  }

  nextPage(event: PageEvent) {
    console.log(event);
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
    this.reloadData();
  }

  deletePresent(id: number) {
    this.presentService.deletePresent(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  presentDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  updatePresent(id: number) {
    this.router.navigate(['update', id]);
  }

  reloadPage() {
    this.reloadData();
  }

}
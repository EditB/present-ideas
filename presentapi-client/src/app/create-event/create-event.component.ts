import { EventService } from '../event.service';
import { Event } from '../event';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs";

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  event: Event = new Event();
  submitted = false;

  events: Observable<Event[]>;

  constructor(private eventService: EventService,
    private router: Router) {
      
     }

  ngOnInit() {
  }

  reloadData() {
    this.eventService.getEventsList().subscribe((data) => {
      console.log(data);
    });
    this.events = this.eventService.getEventsList();
  }

  newEvent(): void {
    this.submitted = false;
    this.event = new Event();
  }

  save() {
    this.eventService.createEvent(this.event)
      .subscribe(data => console.log(data), error => console.log(error));
    this.event = new Event();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
   // this.reloadData();
    this.router.navigate(['/events']);
  }
}



import { Observable } from "rxjs";
import { PersonService } from "../person.service";
import { Person } from "../person";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})


export class PersonListComponent implements OnInit {

  pageIndex: number = 0;
  pageSize: number = 5;
  lowValue: number = 0;
  highValue: number = 5;

  persons: Observable<Person[]>;
  isDataAvailable: boolean = false;

  constructor(private personService: PersonService,
    private router: Router) {
  }

  ngOnInit() {
    this.reloadData().subscribe(
      () => this.isDataAvailable = true

    );
  }

  reloadData() {
    return this.persons = this.personService.getPersonsList();

  }

  nextPage(event: PageEvent) {
    console.log(event);
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
    this.reloadData();
  }

  deletePerson(id: number) {
    this.personService.deletePerson(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  personDetails(id: number) {
    this.router.navigate(['persondetails', id]);
  }

  updatePerson(id: number) {
    this.router.navigate(['updateperson', id]);
  }

  reloadPage() {
    this.reloadData();
  }
}
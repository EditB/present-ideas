export class Person {
    id: number;
    firstName: string;
	lastName: string;
    relation: string;
    birthday: Date;
	emailAddress: string;
	address: string;
    active: boolean;
}
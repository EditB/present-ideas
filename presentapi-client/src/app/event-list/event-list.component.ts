import { Observable } from "rxjs";
import { EventService } from "../event.service";
import { Event } from "../event";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  pageIndex: number = 0;
  pageSize: number = 5;
  lowValue: number = 0;
  highValue: number = 5;

  events: Observable<Event[]>;
  isDataAvailable: boolean = false;

  constructor(private eventService: EventService,
    private router: Router) {
  }

  ngOnInit() {
    this.reloadData().subscribe(
      () => this.isDataAvailable = true
    );
  }

  reloadData() {
    return this.events = this.eventService.getEventsList();
  }

  nextPage(event: PageEvent) {
    console.log(event);
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
    this.reloadData();
  }

  deleteEvent(id: number) {
    this.eventService.deleteEvent(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  updateEvent(id: number) {
    this.router.navigate(['updateevent', id]);
  }

  addEvent() {
    this.router.navigate(['addevent']);
  }

  reloadPage() {
    this.reloadData();
  }
}

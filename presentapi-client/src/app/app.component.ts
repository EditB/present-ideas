import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UpcomingdateListComponent } from './upcomingdate-list/upcomingdate-list.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  todayDate: Date = new Date();
  title = 'My Present Ideas';

  constructor(public dialog: MatDialog){
  
  }
  openDialog() {
    this.dialog.open(UpcomingdateListComponent);
  }
  
}

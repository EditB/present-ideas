import { PresentService } from '../present.service';
import { Present } from '../present';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from "@angular/forms";
import { Observable } from "rxjs";
import { Person } from "../person";
import { PersonService } from "../person.service";
import { EventService } from "../event.service";
import { Event } from "../event";

@Component({
  selector: 'app-create-present',
  templateUrl: './create-present.component.html',
  styleUrls: ['./create-present.component.css']
})
export class CreatePresentComponent implements OnInit {

  present: Present = new Present();
  submitted = false;

  presents: Observable<Present[]>;
  persons: Observable<Person[]>;
  events: Observable<Event[]>;

  constructor(public fb: FormBuilder, private eventService: EventService, private presentService: PresentService, private personService: PersonService,
    private router: Router) {
  }

  personSuitsForm = this.fb.group({
    name: ['']
  })

  eventSuitsForm = this.fb.group({
    name: ['']
  })


  ngOnInit() {
    this.reloadData();
  }

  newPresent(): void {
    this.submitted = false;
    this.present = new Present();
  }

  reloadData() {
    this.presentService.getPresentsList().subscribe((data) => {
      console.log(data);
    });
    this.personService.getPersonsList().subscribe((data) => {
      console.log(data);
    });
    this.eventService.getEventsList().subscribe((data) => {
      console.log(data);
    });
    this.persons = this.personService.getPersonsList();
    this.presents = this.presentService.getPresentsList();
    this.events = this.eventService.getEventsList();

  }
  save() {
    this.presentService.createPresent(this.present)
      .subscribe(data => console.log(data), error => console.log(error));
    this.present = new Present();

    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/presents']);
  }
}

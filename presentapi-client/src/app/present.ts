export class Present {
    id: number;
    person: string;
    gift: string;
    feteevent: string;
    active: boolean;
}
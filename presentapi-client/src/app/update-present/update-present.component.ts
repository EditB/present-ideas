import { Component, OnInit } from '@angular/core';
import { Present } from '../present';
import { ActivatedRoute, Router } from '@angular/router';
import { PresentService } from '../present.service';
import { FormBuilder } from "@angular/forms";
import { PersonService } from "../person.service";
import { EventService } from "../event.service";
import { Observable } from "rxjs";
import { Person } from '../person';
import { Event } from "../event";

@Component({
  selector: 'app-update-present',
  templateUrl: './update-present.component.html',
  styleUrls: ['./update-present.component.css']
})
export class UpdatePresentComponent implements OnInit {

  id: number;
  present: Present;
  presents: Observable<Present[]>;
  persons: Observable<Person[]>;
  events: Observable<Event[]>;

  constructor(public fb: FormBuilder, private route: ActivatedRoute, private eventService: EventService, private personService: PersonService,
    private presentService: PresentService, private router: Router) {
  }

  personSuitsForm = this.fb.group({
    name: ['']
  })

  eventSuitsForm = this.fb.group({
    name: ['']
  })

  ngOnInit() {

    this.present = new Present();

    this.id = this.route.snapshot.params['id'];

    this.presentService.getPresent(this.id)
      .subscribe(data => {
        console.log(data)
        this.present = data;
      this.reloadData();
      }, error => console.log(error));
  }

  reloadData() {
    this.presentService.getPresentsList().subscribe((data) => {
      console.log(data);
    });
    this.personService.getPersonsList().subscribe((data) => {
      console.log(data);
    });
    this.eventService.getEventsList().subscribe((data) => {
      console.log(data);
    });
    this.persons = this.personService.getPersonsList();
    this.presents = this.presentService.getPresentsList();
    this.events = this.eventService.getEventsList();
  }

  updatePresent() {
    this.presentService.updatePresent(this.id, this.present)
      .subscribe(data => console.log(data), error => console.log(error));
    this.present = new Present();
    this.gotoList();
  }

  onSubmit() {
    this.updatePresent();
  }

  gotoList() {
    this.router.navigate(['/presents']);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePresentComponent } from './update-present.component';

describe('UpdatePresentComponent', () => {
  let component: UpdatePresentComponent;
  let fixture: ComponentFixture<UpdatePresentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePresentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePresentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

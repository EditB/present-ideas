import { Component, OnInit } from '@angular/core';
import { Event } from '../event';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';
import { Observable } from "rxjs";

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {

  id: number;
  event: Event;
  events: Observable<Event[]>;
  constructor(private route: ActivatedRoute, private router: Router,
    private eventService: EventService) {
  }

  ngOnInit() {

    this.event = new Event();

    this.id = this.route.snapshot.params['id'];

    this.eventService.getEvent(this.id)
      .subscribe(data => {
        console.log(data)
        this.event = data;
        this.reloadData();
      }, error => console.log(error));
  }

  reloadData() {
    this.events = this.eventService.getEventsList();
  }

  updateEvent() {
    this.eventService.updateEvent(this.id, this.event)
      .subscribe(data => {
        console.log(data);
        this.event = new Event();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateEvent();
  }

  gotoList() {
    this.router.navigate(['/events']);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingdateListComponent } from './upcomingdate-list.component';

describe('UpcomingdateListComponent', () => {
  let component: UpcomingdateListComponent;
  let fixture: ComponentFixture<UpcomingdateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcomingdateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingdateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

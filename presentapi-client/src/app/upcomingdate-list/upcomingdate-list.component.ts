import { Observable } from "rxjs";
import { PersonService } from "../person.service";
import { Person } from "../person";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-upcomingdate-list',
  templateUrl: './upcomingdate-list.component.html',
  styleUrls: ['./upcomingdate-list.component.css']
})
export class UpcomingdateListComponent implements OnInit {

  pageIndex: number = 0;
  pageSize: number = 5;
  lowValue: number = 0;
  highValue: number = 5;

  persons: Observable<Person[]>;
  isDataAvailable: boolean = false;

  constructor(private personService: PersonService,
    private router: Router) {
  }

  ngOnInit() {
    this.reloadData().subscribe(
      () => this.isDataAvailable = true

    );
  }

  reloadData() {
    return this.persons = this.personService.getBirthdayPersonsList();

  }

  nextPage(event: PageEvent) {
    console.log(event);
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    }
    else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
    this.reloadData();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PresentService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/presents';

  constructor(private http: HttpClient) { }

  getPresent(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createPresent(present: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, present);
  }

  updatePresent(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletePresent(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getPresentsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  
  
  getPerson(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createPerson(person: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, person);
  }

  updatePerson(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletePerson(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getPersonsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  
}

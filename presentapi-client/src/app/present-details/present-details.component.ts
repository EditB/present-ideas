import { Present } from '../present';
import { Component, OnInit, Input } from '@angular/core';
import { PresentService } from '../present.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-present-details',
  templateUrl: './present-details.component.html',
  styleUrls: ['./present-details.component.css']
})
export class PresentDetailsComponent implements OnInit {

  id: number;
  present: Present;

  constructor(private route: ActivatedRoute, private router: Router,
    private presentService: PresentService) { }

  ngOnInit() {
    this.present = new Present();

    this.id = this.route.snapshot.params['id'];

    this.presentService.getPresent(this.id)
      .subscribe(data => {
        console.log(data)
        this.present = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['presents']);
  }
}
export class Event {
    id: number;
    eventName: string;
    active: boolean;
}
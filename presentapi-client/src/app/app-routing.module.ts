import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PresentDetailsComponent } from './present-details/present-details.component';
import { CreatePresentComponent } from './create-present/create-present.component';
import { PresentListComponent } from './present-list/present-list.component';
import { UpdatePresentComponent } from './update-present/update-present.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { CreatePersonComponent } from './create-person/create-person.component';
import { PersonListComponent } from './person-list/person-list.component';
import { UpdatePersonComponent } from './update-person/update-person.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventListComponent } from './event-list/event-list.component';
import { UpdateEventComponent } from './update-event/update-event.component';


const routes: Routes = [
  { path: '', redirectTo: 'present', pathMatch: 'full' },
  { path: 'presents', component: PresentListComponent },
  { path: 'add', component: CreatePresentComponent },
  { path: 'update/:id', component: UpdatePresentComponent },
  { path: 'details/:id', component: PresentDetailsComponent },

  { path: 'persons', component: PersonListComponent },
  { path: 'addperson', component: CreatePersonComponent },
  { path: 'updateperson/:id', component: UpdatePersonComponent },
  { path: 'persondetails/:id', component: PersonDetailsComponent },

  { path: 'events', component: EventListComponent },
  { path: 'addevent', component: CreateEventComponent },
  { path: 'updateevent/:id', component: UpdateEventComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
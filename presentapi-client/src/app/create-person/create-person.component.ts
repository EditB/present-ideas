import { PersonService } from '../person.service';
import { Person } from '../person';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from "@angular/forms";
import { Observable } from "rxjs";

import { MatDatepickerModule, MatInputModule, MatNativeDateModule } from '@angular/material';

@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.css']
})
export class CreatePersonComponent implements OnInit {

  relationSuits: any = ['Family', 'Friend', 'Co-workers', 'Business Partner', 'Other']

  person: Person = new Person();
  submitted = false;

  persons: Observable<Person[]>;

  constructor(public fb: FormBuilder, private personService: PersonService,
    private router: Router) {
  }

  relationSuitsForm = this.fb.group({
    name: ['']
  })

  ngOnInit() {

  }

  newPerson(): void {
    this.submitted = false;
    this.person = new Person();
  }

  save() {
    this.personService.createPerson(this.person)
      .subscribe(data => console.log(data), error => console.log(error));
    this.person = new Person();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/persons']);
  }
}
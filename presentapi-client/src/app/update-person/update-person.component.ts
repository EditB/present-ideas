import { Component, OnInit } from '@angular/core';
import { Person } from '../person';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonService } from '../person.service';
import { FormBuilder } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: 'app-update-person',
  templateUrl: './update-person.component.html',
  styleUrls: ['./update-person.component.css']
})
export class UpdatePersonComponent implements OnInit {

  relationSuits: any = ['Family', 'Friend', 'Co-workers', 'Business Partner', 'Other']

  id: number;
  person: Person;
  persons: Observable<Person[]>;

  constructor(public fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private personService: PersonService) {
  }

  relationSuitsForm = this.fb.group({
    name: ['']
  })

  ngOnInit() {
    this.person = new Person();

    this.id = this.route.snapshot.params['id'];

    this.personService.getPerson(this.id)
      .subscribe(data => {
        console.log(data)
        this.person = data;
      }, error => console.log(error));
  }

  updatePerson() {
    this.personService.updatePerson(this.id, this.person)
      .subscribe(data => {
        console.log(data);
        this.person = new Person();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updatePerson();
  }

  gotoList() {
    this.router.navigate(['/persons']);
  }
}

1. Clone the project via git bash:
cd your directory
git init
git clone https://gitlab.com/EditB/present-ideas.git

2. Database:
In your database create new schema called 'presentapi'.
(E.g. in MySQL:  
CREATE SCHEMA `presentapi` ;)

3. Back-end: 
-Import the project 'presentapi' in your IDE.
(E.g. in Netbeans IDE you can import the zipped version of presentapi. File\export project zip)

-Update the file '/presentapi/src/main/resources/application.properties'.
(E.g. this lines:
spring.datasource.url = ....
spring.datasource.username = ....
spring.datasource.password = ......)

-Maven Build/Install

-Run application in PresentapiApplication.java

To check everything works properly, open your database and see if the new tables exsist in presentapi schema: events, hibernate_sequence, presents, persons.  

4. Front-end:
-You need Angular 8 or higher version. If you don't have yet:
install node.js, than open node.js command prompt and install Angular CLI.

-To run the front-end:
cd your directory\present-ideas\presentapi-client>
npm install
ng serve


5. Open your Chrome browser : http://localhost:4200/presents
You will see the application with empty Present List.
Fill your Event List (Edit Events) and your Person List (Add Person). Now you can collect your gift ideas in the Present List (Add Present).
You can check the values in your database.

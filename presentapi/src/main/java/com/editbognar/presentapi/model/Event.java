package com.editbognar.presentapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "events")
public class Event {

	private long id; // TODO: connect to present.person
	private String eventName;
	
	public Event() {
		
	}
	
	public Event(long id, String eventName) {
		super();
		this.id = id;
		this.eventName = eventName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "event_name", nullable = false)
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", eventName=" + eventName + "]";
	}

	
}

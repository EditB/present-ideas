package com.editbognar.presentapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "presents")
public class Present {

	private long id;
	private String person;
	private String gift;
	private String feteevent;

	public Present() {

	}

	public Present(String person, String gift, String feteevent) {
		super();
		this.person = person;
		this.gift = gift;
		this.feteevent = feteevent;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "person_whom", nullable = false)
	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	@Column(name = "gift_what", nullable = false)
	public String getGift() {
		return gift;
	}

	public void setGift(String gift) {
		this.gift = gift;
	}

	@Column(name = "event_for", nullable = false)
	public String getFeteevent() {
		return feteevent;
	}

	public void setFeteevent(String feteevent) {
		this.feteevent = feteevent;
	}

	@Override
	public String toString() {
		return "Present [id=" + id + ", person=" + person + ", gift=" + gift + ", feteevent=" + feteevent + "]";
	}

}

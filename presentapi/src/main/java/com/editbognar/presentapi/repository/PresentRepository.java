package com.editbognar.presentapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.editbognar.presentapi.model.Present;

@Repository
public interface PresentRepository extends JpaRepository<Present, Long>,PagingAndSortingRepository<Present, Long>{

}

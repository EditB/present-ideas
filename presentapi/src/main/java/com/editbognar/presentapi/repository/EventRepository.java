package com.editbognar.presentapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.editbognar.presentapi.model.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>{

}

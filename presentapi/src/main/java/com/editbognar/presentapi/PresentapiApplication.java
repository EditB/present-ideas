package com.editbognar.presentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PresentapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PresentapiApplication.class, args);
	}

}

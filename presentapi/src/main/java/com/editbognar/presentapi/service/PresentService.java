package com.editbognar.presentapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.editbognar.presentapi.model.Present;
import com.editbognar.presentapi.repository.PresentRepository;

@Service
public class PresentService 
{
    @Autowired
    PresentRepository repository;
     
    public List<Present> getAllPresents(Integer pageNo, Integer pageSize)
    {
        Pageable paging = PageRequest.of(pageNo, pageSize);
 
        Page<Present> pagedResult = repository.findAll(paging);
         
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Present>();
        }
    }

}

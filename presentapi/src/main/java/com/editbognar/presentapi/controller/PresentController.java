package com.editbognar.presentapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.editbognar.presentapi.exception.ResourceNotFoundException;
import com.editbognar.presentapi.model.Person;
import com.editbognar.presentapi.model.Present;
import com.editbognar.presentapi.repository.PersonRepository;
import com.editbognar.presentapi.repository.PresentRepository;
import com.editbognar.presentapi.service.PresentService;

import static java.util.stream.Collectors.toList;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class PresentController {
	@Autowired
	private PresentRepository presentRepository;
	
	@Autowired
    PresentService service;
	
	@GetMapping("/presents")
	public List<Present> getAllPersons() {
		return presentRepository.findAll();
	}
	
	@GetMapping("/presents/{id}")
	public ResponseEntity<Present> getPresentById(@PathVariable(value = "id") Long presentId)
			throws ResourceNotFoundException {
		Present present = presentRepository.findById(presentId)
				.orElseThrow(() -> new ResourceNotFoundException("Present not found for this id :: " + presentId));
		return ResponseEntity.ok().body(present);
	}

	@PostMapping("/presents")
	public Present createPresent(@Valid @RequestBody Present present) {
		return presentRepository.save(present);
	}

	@PutMapping("/presents/{id}")
	public ResponseEntity<Present> updatePresent(@PathVariable(value = "id") Long presentId,
			@Valid @RequestBody Present presentDetails) throws ResourceNotFoundException {
		Present present = presentRepository.findById(presentId)
				.orElseThrow(() -> new ResourceNotFoundException("Present not found for this id :: " + presentId));

		present.setGift(presentDetails.getGift());
		present.setPerson(presentDetails.getPerson());
		present.setFeteevent(presentDetails.getFeteevent());

		final Present updatedPresent = presentRepository.save(present);
		return ResponseEntity.ok(updatedPresent);
	}

	@DeleteMapping("/presents/{id}")
	public Map<String, Boolean> deletePresent(@PathVariable(value = "id") Long presentId)
			throws ResourceNotFoundException {
		Present present = presentRepository.findById(presentId)
				.orElseThrow(() -> new ResourceNotFoundException("Present not found for this id :: " + presentId));

		presentRepository.delete(present);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}

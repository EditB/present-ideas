package com.editbognar.presentapi.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.editbognar.presentapi.exception.ResourceNotFoundException;
import com.editbognar.presentapi.model.Person;
import com.editbognar.presentapi.repository.PersonRepository;
import com.editbognar.presentapi.service.PersonService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class PersonController {
	@Autowired
	private PersonRepository personRepository;

	@Autowired
	PersonService service;

	@GetMapping("/persons")
	public List<Person> getAllPersons() {
		return personRepository.findAll();
	}

	@GetMapping("/persons/{id}")
	public ResponseEntity<Person> getPersonById(@PathVariable(value = "id") Long personId)
			throws ResourceNotFoundException {
		Person person = personRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));
		return ResponseEntity.ok().body(person);
	}

	@PostMapping("/persons")
	public Person createPerson(@Valid @RequestBody Person person) {
		return personRepository.save(person);
	}

	@PutMapping("/persons/{id}")
	public ResponseEntity<Person> updatePerson(@PathVariable(value = "id") Long personId,
			@Valid @RequestBody Person personDetails) throws ResourceNotFoundException {
		Person person = personRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));

		person.setFirstName(personDetails.getFirstName());
		person.setLastName(personDetails.getLastName());
		person.setRelation(personDetails.getRelation());
		person.setBirthday(personDetails.getBirthday());
		person.setEmailAddress(personDetails.getEmailAddress());
		person.setAddress(personDetails.getAddress());

		final Person updatedPerson = personRepository.save(person);
		return ResponseEntity.ok(updatedPerson);
	}

	@DeleteMapping("/persons/{id}")
	public Map<String, Boolean> deletePerson(@PathVariable(value = "id") Long personId)
			throws ResourceNotFoundException {
		Person person = personRepository.findById(personId)
				.orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));

		personRepository.delete(person);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@GetMapping("/persons/bd")
	public List<Person> getBirthdayPersons() {
		List<Person> bdPerson = personRepository.findAll();
		List<Person> upcomingBdPerson = new ArrayList<>();
		for (int i = 0; i < bdPerson.size(); i++) {
			Calendar calPerson = Calendar.getInstance();
			calPerson.setTime(bdPerson.get(i).getBirthday());
			Calendar calNow = Calendar.getInstance();
			Calendar cal = Calendar.getInstance();
			
			int year = cal.get(Calendar.YEAR);
			if(cal.get(Calendar.MONTH)==Calendar.DECEMBER && calPerson.get(Calendar.MONTH)==Calendar.JANUARY) {		
				calPerson.set(Calendar.YEAR, year+1);
			} else {
				calPerson.set(Calendar.YEAR, year);
			}
			cal.add(Calendar.MONTH, 1);
			if (calPerson.before(cal) && calPerson.after(calNow)) {
				upcomingBdPerson.add(bdPerson.get(i));
			}
		}
		return upcomingBdPerson;
	}

}

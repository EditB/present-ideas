package com.editbognar.presentapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.editbognar.presentapi.exception.ResourceNotFoundException;
import com.editbognar.presentapi.model.Event;
import com.editbognar.presentapi.repository.EventRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class EventController {

	@Autowired
	private EventRepository eventRepository;
	
	@GetMapping("/events")
	public List<Event> getAllEvents() {
		return eventRepository.findAll();
	}

	@GetMapping("/events/{id}")
	public ResponseEntity<Event> getEventById(@PathVariable(value = "id") Long eventId)
			throws ResourceNotFoundException {
		Event event = eventRepository.findById(eventId)
				.orElseThrow(() -> new ResourceNotFoundException("Event not found for this id :: " + eventId));
		return ResponseEntity.ok().body(event);
	}

	@PostMapping("/events")
	public Event createEvent(@Valid @RequestBody Event event) {
		return eventRepository.save(event);
	}

	@PutMapping("/events/{id}")
	public ResponseEntity<Event> updateEvent(@PathVariable(value = "id") Long eventId,
			@Valid @RequestBody Event eventDetails) throws ResourceNotFoundException {
		Event event = eventRepository.findById(eventId)
				.orElseThrow(() -> new ResourceNotFoundException("Event not found for this id :: " + eventId));

		event.setEventName(eventDetails.getEventName());
		
		final Event updatedEvent = eventRepository.save(event);
		return ResponseEntity.ok(updatedEvent);
	}

	@DeleteMapping("/events/{id}")
	public Map<String, Boolean> deleteEvent(@PathVariable(value = "id") Long eventId)
			throws ResourceNotFoundException {
		Event event = eventRepository.findById(eventId)
				.orElseThrow(() -> new ResourceNotFoundException("Event not found for this id :: " + eventId));

		eventRepository.delete(event);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
